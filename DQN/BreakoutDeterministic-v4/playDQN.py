import pdb                                                                                                                                                                                                
import sys 
import os

gym_path = os.path.join(os.environ['HOME'], "local","gym")
sys.path.append(gym_path)

import torch
import torch.nn as nn
import numpy as np
import gym 
from collections import namedtuple
from itertools import count
import random
import math
import copy
#import matplotlib.pyplot as plt

if torch.cuda.is_available():
    device="cuda:0"
else:
    device="cpu"




class ConvNet(nn.Module):

    def __init__(self):
        super(ConvNet, self).__init__()
        self.i_h1 = nn.Sequential(nn.Conv2d(4,16,kernel_size=8, stride=4),nn.ReLU())
        self.h1_h2 = nn.Sequential(nn.Conv2d(16,32,kernel_size=4, stride=2),nn.ReLU())
        self.h2_h3 = nn.Sequential(nn.Linear(2816, 256), nn.ReLU())
        self.h3_o = nn.Linear(256,4)
    
    def forward(self, x): 
        h1 = self.i_h1(x)
        h2 = self.h1_h2(h1).view(x.shape[0], -1) 
        h3 = self.h2_h3(h2)
        output = self.h3_o(h3)
        return output

    

def to_grayscale(img):
    return np.mean(img, axis=2).astype(np.uint8)

def downsample(img):
    return img[::2, ::2]



def preprocess(img):
    return to_grayscale(downsample(img))


Transition = namedtuple('Transition',
                        ('state', 'action', 'next_state', 'reward'))

Q_value_function_net = ConvNet()
Q_value_function_net.to(device)

PATH_RESULTS = os.path.join(os.environ['HOME'], 'DL-RL/DQN/BreakoutDeterministic-v4/trained-models')
state = torch.load(os.path.join(PATH_RESULTS, 'model_episode_19900.pth.tar'))


Q_value_function_net.load_state_dict(state['state_dict'])


#plt.plot(episode_duration)
#plt.plot(episode_reward)
#np.unique(np.array(episode_reward), return_counts=True)
#
#
#
## we play with the learned strategy
#env.close()
env = gym.make('BreakoutDeterministic-v4')
old_observation = env.reset()
old_observation = preprocess(old_observation)
old_observation = torch.tensor(old_observation, dtype=torch.uint8, device=device).unsqueeze(0)
buffer_obs = [old_observation]
done = False
R = 0



Q_value_function_net.eval()
length = 1
t=0
list_actions = []
done = False
EPSILON = 0.01

while not done:
    #env.render()
    if len(buffer_obs)==4:
        state = torch.cat(buffer_obs, 0)
        state = state.new_tensor(state, dtype=torch.float)
        state = (state-128)/256
        unif = random.random()
        if unif>EPSILON:
            with torch.no_grad():
                action = Q_value_function_net(state.unsqueeze(0)).max(1)[1].view(1, 1)
        else:
            action = env.action_space.sample()
            action = torch.tensor([[action]])
    else:
        action = env.action_space.sample()
        action = torch.tensor([[action]])
    #action = env.action_space.sample()
    #action = torch.tensor([[action]])
    list_actions.append(action)
    new_observation, reward, done, _ = env.step(action.cpu().item())
    new_observation = preprocess(new_observation)
    new_observation = torch.tensor(new_observation, dtype=torch.uint8, device=device).unsqueeze(0)
    buffer_obs.append(new_observation)
    if len(buffer_obs)==5:
        del buffer_obs[0]
    
    R += reward
    t += 1
    print("Action: {}, R: {}, reward: {}, t: {}".format(action.item(), R, reward, t))
        
actions = torch.cat(list_actions)
actions = torch.tensor(actions, dtype=torch.float)
np.unique(actions.numpy(), return_counts=True)    



