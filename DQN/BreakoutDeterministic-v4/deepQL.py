import pdb
import sys
import os

gym_path = os.path.join(os.environ['HOME'], "local","gym")
sys.path.append(gym_path)

import torch
import torch.nn as nn
import numpy as np
import gym
from collections import namedtuple
from itertools import count
import random
import math
import copy
#import matplotlib.pyplot as plt

if torch.cuda.is_available():
    device="cuda:0"
else:
    device="cpu"




class ConvNet(nn.Module):

    def __init__(self):
        super(ConvNet, self).__init__()
        self.i_h1 = nn.Sequential(nn.Conv2d(4,16,kernel_size=8, stride=4),nn.ReLU())
        self.h1_h2 = nn.Sequential(nn.Conv2d(16,32,kernel_size=4, stride=2),nn.ReLU())
        self.h2_h3 = nn.Sequential(nn.Linear(2816, 256), nn.ReLU())
        self.h3_o = nn.Linear(256,4)
    
    def forward(self, x):
        h1 = self.i_h1(x)
        h2 = self.h1_h2(h1).view(x.shape[0], -1)
        h3 = self.h2_h3(h2)
        output = self.h3_o(h3)
        return output

    

def to_grayscale(img):
    return np.mean(img, axis=2).astype(np.uint8)

def downsample(img):
    return img[::2, ::2]

def preprocess(img):
    return to_grayscale(downsample(img))


Transition = namedtuple('Transition',
                        ('state', 'action', 'next_state', 'reward'))


class ReplayMemory(object):

    def __init__(self, capacity):
        self.capacity = capacity
        self.memory = []
        self.position = 0

    def push(self, *args):
        """Saves a transition."""
        if len(self.memory) < self.capacity:
            self.memory.append(None)
        self.memory[self.position] = Transition(*args)
        self.position = (self.position + 1) % self.capacity

    def sample(self, batch_size):
        return random.sample(self.memory, batch_size)

    def __len__(self):
        return len(self.memory)
    

BATCH_SIZE = 32 
GAMMA = 0.99
EPS_START = 1
EPS_END = 0.1
EPS_DECAY = 1000000
TARGET_UPDATE = 10000
MEMORY_SIZE = 200000
UPDATE_FREQUENCY = 4




#BATCH_SIZE = 200
#GAMMA = 0.999
#EPS_START = 1
#EPS_END = 0.1
#EPS_DECAY = 100000
#TARGET_UPDATE = 10


def select_action(state):
    """Epsilon greedy"""
    global steps_done
    sample = random.random()
    
    # epsilon decay
    #eps_threshold = EPS_END + (EPS_START - EPS_END) * math.exp(-1*steps_done / EPS_DECAY)
    lambda_ = min([steps_done,EPS_DECAY])/EPS_DECAY
    eps_threshold = lambda_ * EPS_END + (1-lambda_)*EPS_START
    steps_done += 1
    
    if sample > eps_threshold:
        with torch.no_grad():
            # t.max(1) will return largest column value of each row.
            # second column on max result is index of where max element was
            # found, so we pick action with the larger expected reward.
            state = state.new_tensor(data=state, dtype=torch.float)
            state = (state-128)/256
            #state = torch.tensor(state, device=device, dtype=torch.float)
            action = Q_value_function_net(state).max(1)[1].view(1, 1)
            return action.new_tensor(action, dtype=torch.uint8)
    else:
        return torch.tensor([[random.randrange(4)]], device=device, dtype=torch.uint8)



#env.close()
env = gym.make('BreakoutDeterministic-v4')
#env.reset()
#print(env.action_space)
#print(env.observation_space)
#print(env.observation_space.high)
#print(env.observation_space.low)
#list_reward = []
#R = 0
#for _ in range(1000):
#    env.render()
#    observation, reward, done, info = env.step(env.action_space.sample()) # take a random action
#    list_reward.append(reward)
#env.close()





def optimize_model():
    target_net.eval()
    Q_value_function_net.train()
    
    # debugging
    #pdb.set_trace()
    
    optimizer.zero_grad()
        
    if len(memory)<BATCH_SIZE:
        return
    
    transitions = memory.sample(BATCH_SIZE)
    batch = Transition(*zip(*transitions))
    
    non_final_mask = torch.tensor([s is not None for s in batch.next_state], device=device, dtype=torch.uint8)
    
    non_final_next_states = torch.stack([s for s in batch.next_state if s is not None], 0)
    #non_final_next_states = torch.tensor(non_final_next_states, device=device,dtype=torch.float)
    non_final_next_states = non_final_next_states.new_tensor(non_final_next_states, dtype=torch.float)
    non_final_next_states = (non_final_next_states-128)/256
    #pdb.set_trace()
    state_batch = torch.stack(batch.state, 0)
    state_batch = state_batch.new_tensor(state_batch, dtype=torch.float)
    state_batch = (state_batch-128)/256
    #state_batch = torch.tensor(state_batch, device=device,dtype=torch.float)
    action_batch = torch.cat(batch.action)
    action_batch = action_batch.new_tensor(action_batch, dtype=torch.int64)
    reward_batch = torch.cat(batch.reward)
    
    state_action_values = Q_value_function_net(state_batch).gather(1, action_batch.view(-1,1))
    
    # Compute V(s_{t+1}) for all next states.
    # Expected values of actions for non_final_next_states are computed based
    # on the "older" target_net; selecting their best reward with max(1)[0].
    # This is merged based on the mask, such that we'll have either the expected
    # state value or 0 in case the state was final.
    next_state_values = torch.zeros(BATCH_SIZE, device=device)    
    next_state_values[non_final_mask] = target_net(non_final_next_states).max(1)[0].detach()
    
    # Compue the expected Q values
    expected_state_action_values = (next_state_values * GAMMA) + reward_batch
    
    # Compute Loss
    loss = loss_fn(state_action_values, expected_state_action_values.view(-1,1))
    loss.backward()
    optimizer.step()
    return loss.item()
    





if __name__ == '__main__':
    Q_value_function_net = ConvNet()
    Q_value_function_net.to(device) 
    target_net = ConvNet()
    target_net.to(device)
    target_net.load_state_dict(Q_value_function_net.state_dict())
    target_net.eval()
    
    #optimizer = torch.optim.Adam(Q_value_function_net.parameters(), lr=0.00025, betas=[0.95,0.95], eps=0.01)
    optimizer = torch.optim.RMSprop(Q_value_function_net.parameters(), lr=0.00025, momentum=0.95, alpha=0, eps=0.01)
    #loss_fn = nn.MSELoss()
    loss_fn = nn.SmoothL1Loss()
    memory = ReplayMemory(MEMORY_SIZE)
    memory_start_size = 50000
    
    steps_done = 0
    number_param_updates = 0

    num_episodes = 20000
    episode_duration = []
    episode_reward = []
    for episode in range(num_episodes):
        old_observation = env.reset()
        old_observation = preprocess(old_observation)
        old_observation = torch.tensor(old_observation, dtype=torch.uint8, device=device).unsqueeze(0)#.pin_memory()
        #old_observation = torch.as_tensor(old_observation, dtype=torch.uint8, device=device).view(1,-1)
        buffer_obs = [old_observation]
        done = False
        R = 0
        for t in count():
            #print("t={}".format(t))
            #print(old_observation.shape)
            Q_value_function_net.eval()
            if len(buffer_obs)==4 and len(memory)>memory_start_size:
                #pdb.set_trace()
                state = torch.cat(buffer_obs, 0)
                action = select_action(state.unsqueeze(0))
                
            else:
                action = env.action_space.sample()
                action = torch.tensor([[action]], dtype=torch.uint8, device=device)
                
            Q_value_function_net.train()
            
            new_observation, reward, done, _ = env.step(action.item())
            R += reward
            #new_observation = torch.as_tensor(new_observation, dtype=torch.uint8, device=device).view(1,-1)
            new_observation = preprocess(new_observation)
            new_observation = torch.tensor(new_observation, dtype=torch.uint8, device=device).unsqueeze(0)#.pin_memory()#permute(2,0,1)
            reward = torch.tensor([reward], device=device)
            buffer_obs.append(new_observation)
            
            # store the transition in memory
            if not done and len(buffer_obs)==5:
                #if t%4 == 0:
                memory.push(torch.cat(buffer_obs[:-1],0), action, torch.cat(buffer_obs[1:],0), reward)
                del buffer_obs[0] # delete the oldest element of buffer_obs
            elif done:
                #pdb.set_trace()
                memory.push(torch.cat(buffer_obs[:-1],0), action, None, reward)
            
            # one step of the optimization
            if len(memory)>memory_start_size:
                lambda_ = min([steps_done,EPS_DECAY])/EPS_DECAY
                eps_threshold = lambda_ * EPS_END + (1-lambda_)*EPS_START
                PATH_RESULTS = os.path.join(os.environ['HOME'], 'DL-RL', 'DQN/BreakoutDeterministic-v4/trained-models')
                if t % UPDATE_FREQUENCY == 0:
                    loss = optimize_model()
                    number_param_updates += 1
                    if number_param_updates % TARGET_UPDATE == 0:
                        target_net.load_state_dict(copy.deepcopy(Q_value_function_net.state_dict()))
                        with open(os.path.join(PATH_RESULTS, 'log.txt'),'a') as f:
                            f.write('target network updated!!!!!\n')

                    #if t%10 == 0:
                    with open(os.path.join(PATH_RESULTS, 'log.txt'), 'a') as f:
                        f.write("Episode: {}/{}, timestep: {}, R: {}, steps_done: {}, epsilon: {:.2f}, updates: {}, loss: {:.3f}\n".format(episode, num_episodes, t+1, R, steps_done, eps_threshold, number_param_updates, loss))
                else:
                    with open(os.path.join(PATH_RESULTS, 'log.txt'), 'a') as f:
                        f.write("Episode: {}/{}, timestep: {}, R: {}, steps_done: {}, epsilon: {:.2f}, updates: {}\n".format(episode, num_episodes, t+1, R, steps_done, eps_threshold, number_param_updates))


            
            if done:
                episode_duration.append(t+1)
                episode_reward.append(R)
                break
            

        if (episode+1) % 100 == 0: 
            state = {"state_dict":Q_value_function_net.state_dict()}
            PATH_RESULTS = os.path.join(os.environ['HOME'], 'DL-RL', 'DQN/BreakoutDeterministic-v4/trained-models')
            torch.save(state, os.path.join(PATH_RESULTS, 'model_episode_'+str(episode+1)+'.pth.tar'))
            
            
        
     
#plt.plot(episode_duration)
#plt.plot(episode_reward)
#np.unique(np.array(episode_reward), return_counts=True)
#
#
#
## we play with the learned strategy
#env.close()
#env = gym.make('Breakout-ram-v0')
#old_observation = env.reset()
#old_observation = torch.tensor(old_observation, dtype=torch.uint8, device=device).view(1,-1)
#buffer_obs = [old_observation]
#done = False
#R = 0
#Q_value_function_net.eval()
#length = 1
#list_actions = []
#for t in count():
#    env.render()
#    if len(buffer_obs)==4:
#        state = torch.cat(buffer_obs, 1)
#        with torch.no_grad():
#            action = Q_value_function_net(old_observation).max(1)[1].view(1, 1)
#    else:
#        action = env.action_space.sample()
#        action = torch.tensor([[action]])
#    list_actions.append(action)
#    new_observation, reward, done, _ = env.step(action.item())
#    old_observation = torch.tensor(new_observation, dtype=torch.float, device=device).view(1,-1)        
#    R += reward
#    length += 1
#    print("Action: {}, R: {}, reward: {}, t: {}".format(action.item(), R, reward, t))
#    if done:
#        break
#        
#actions = torch.cat(list_actions)
#actions = torch.tensor(actions, dtype=torch.float)
#np.unique(actions.numpy(), return_counts=True)    

    
    










    


    
